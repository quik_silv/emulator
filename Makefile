CC=gcc
CFLAGS=-I.

emulator: emulator.c
	$(CC) -o emulator emulator.c $(CFLAGS)

.PHONY: clean

clean:
	rm -f *~ emulator
