/*
    @file emulator.c
    @author Mark Wong
    @date 16 Nov 2016
    @brief RECBE emulator to test DAQ readout code.
    
    Starts a server in localhost at port 8888 where the DAQ client can connect to and
    obtain the emulated data. The data is formatted in unsigned short and here is
    and example of the partial data which adheres to the RECBE data packet format.
    unsigned short client_message[] = {
        0x0022, 0x0000, 0x4079, 0x0018, 0x0000, 0x0000, 
        0xdf00, 0xdd00, 0xdf00, 0xdd00, 0xe100, 0xe200, 
        0xe800, 0xee00, 0xdc00, 0xdc00, 0xdb00, 0xdd00,
        0xda00, 0xdf00, 0xe100, 0xeb00, 0xda00, 0xdb00, 
        0xdb00, 0xdd00, 0xdf00, 0xe000, 0xe500, 0xea00, 
*/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>    //strlen
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <unistd.h>    //write
#include <netinet/in.h>

#define PACKET_SIZE 6156
unsigned short data[PACKET_SIZE];
typedef int bool;
#define true 1
#define false 0
void readFromFile(const char *);
int main(int argc , char *argv[])
{
    unsigned short header[] = {
        0x0022, 0x0000, 0xe052, 0x0018, 0x0000, 0x0000 
    };
    readFromFile("test.txt");
    int socket_desc , client_sock , c , read_size;
    struct sockaddr_in server , client;
     
    //Create socket
    socket_desc = socket(AF_INET , SOCK_STREAM , IPPROTO_TCP);
    if (socket_desc == -1)
    {
        printf("Could not create socket");
    }
    puts("Socket created");
     
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 8888 );
     
    // lose the pesky "address already in use" error message
    int yes=1;
    setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

    //Bind
    if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        //print the error message
        perror("Bind failed. Error");
        return 1;
    }
    puts("Bind done");
     
    //Listen
    listen(socket_desc , 3);
     
    //Accept and incoming connection
    puts("Waiting for incoming connections...");
    c = sizeof(struct sockaddr_in);
    int counter =0; 
    while(1) {
        //accept connection from an incoming client
        client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
        if (client_sock < 0)
        {
            perror("Accept failed");
            continue;
        }
        puts("Connection accepted");
        ssize_t ret;
//        ret = write(client_sock, header, 12);
        ret = write(client_sock, data, PACKET_SIZE);
        if(ret == -1) perror("Write failed.");
        close(client_sock);
    }
    return 0;
}
void readFromFile(const char * filename)
{
    FILE * fp;
    char * line = NULL;
    char * newline = NULL;
    size_t len = 0;
    ssize_t read;
    int i = 0;

    unsigned short temp;
    char * token;

    fp = fopen(filename, "r");
    if (fp == NULL)
        printf("File does not exist!\n");

    while ((read = getline(&line, &len, fp)) != -1) {
        if (strlen(line) == 1) continue;
        newline = strchr(line, '\n');
        if (newline) *newline = 0;
        token = strtok(line, " ");
        while( token != NULL)
        {
            sscanf(token, "%hx", &temp);
            //temp = strtol(token, NULL, 16);
            data[i] = temp;
            token = strtok(NULL, " ");
            i++;
        }
    }

    fclose(fp);
}
